package br.com.caelum.estoque.cliente;

public class TesteServicoWeb {

	public static void main(String[] args) {
		
		EstoqueWS client = new EstoqueWS_Service().getEstoqueWSPort();
		
		Filtros filtros = new Filtros();
		Filtro filtro = new Filtro();
		
		filtro.setNome("Teste");
		filtros.getFiltro().add(filtro);
		
		ListaItens lista =  client.todosOsItens(filtros);

	}

}
