package br.com.caelum.estoque.ws;

import java.util.Arrays;

import javax.jws.WebService;

@WebService(endpointInterface="br.com.caelum.estoque.ws.EstoqueWS", serviceName="EstoqueWS", portName="EstoqueWSPort")
public class EstoqueWSImpl implements EstoqueWS {
	
	/**
	 * 
	 * Para gerar um servico apartir do wsdl
		wsconsume.bat -k -n -o C:/ambiente/WorkSpaces/JAX-WS-SOAP/estoquews-web/src/ 
		C:/ambiente/WorkSpaces/JAX-WS-SOAP/estoquews-web/src/EstoqueWSServiceCap5.wsdl
		
		Para gerar os clients
		
		wsimport -s src -p br.com.caelum.estoque.cliente http://localhost:8080/estoquews-web/EstoqueWS?wsdl
	 */
	
	@Override
	public ListaItens todosOsItens(Filtros filtros) {
		System.out.println("Chamando todos os Itens");
		ListaItens listaItens = new ListaItens();
		listaItens.item = Arrays.asList(geraItem());
		return listaItens;
	}

	@Override
	public CadastrarItemResponse cadastrarItem(CadastrarItem parameters, TokenUsuario tokenUsuario)
			throws AutorizacaoFault {
		System.out.println("Chamando cadastarItem");
		CadastrarItemResponse resposta = new CadastrarItemResponse();
		resposta.setItem(geraItem());
		return resposta;
	}
	
	private Item geraItem() {
		Item item = new Item();
		item.codigo = "MEA";
		item.nome = "MEAN";
		item.quantidade = 5;
		item.tipo = "Livro";
		return item;
	}

}
