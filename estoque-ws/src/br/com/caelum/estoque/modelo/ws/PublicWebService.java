package br.com.caelum.estoque.modelo.ws;

import javax.xml.ws.Endpoint;

public class PublicWebService {

	public static void main(String[] args) {
		
		EstoqueWS servico = new EstoqueWS();
		String url ="http://localhost:8080/estoquews";
		
		Endpoint.publish(url, servico);
		
	}

}
