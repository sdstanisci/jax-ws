package br.com.caelum.estoque.modelo.ws;

import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;
import javax.security.sasl.AuthenticationException;

import br.com.caelum.estoque.modelo.item.Filtro;
import br.com.caelum.estoque.modelo.item.Filtros;
import br.com.caelum.estoque.modelo.item.Item;
import br.com.caelum.estoque.modelo.item.ItemDao;
import br.com.caelum.estoque.modelo.item.ListaItens;
import br.com.caelum.estoque.modelo.usuario.AutorizacaoException;
import br.com.caelum.estoque.modelo.usuario.TokenDao;
import br.com.caelum.estoque.modelo.usuario.TokenUsuario;

@WebService
@SOAPBinding(style=Style.DOCUMENT, use=Use.LITERAL, parameterStyle=ParameterStyle.WRAPPED)
public class EstoqueWS {// RPC - mesmo usando um document, estamos usando parametro wrapped no estilo 
						// para deixar o metodo explicito no soapui
						// se trocar o wrapped por bare teremos o metodo oculto no soapui
	/**
	 * Para gerar o wsdl 
	 * wsgen -cp bin -d bin br.com.caelum.estoque.modelo.ws.EstoqueWS -wsdl -inlineSchemas
	 
	 Para gerar os clients apartir de uma arquivo com ex wsdl 
	 Precisa ir ate a pasta do Wildfly bin e usar o wsconsume
	 
	 wsconsume.bat -k -n -o C:\ambiente\WorkSpaces\JAX-WS-SOAP-NETBEANS-WEB\src C:\ambiente\WorkSpaces\JAX-WS-SOAP-NETBEANS-WEB\src\EstoqueWSService.wsdl
	 
	 Para gerar os o client apartir de um WSDL exposto
	 
	 wsimport -s src -p br.com.caelum.estoque.ws.cliente http://localhost:8080/JAX-WS-SOAP-NETBEANS-WEB/EstoqueWSImpl?wsdl
	 
	 */
	
	private ItemDao dao = new ItemDao();
	
	@WebMethod(operationName="todosItens")
	@WebResult(name="itens")
	public ListaItens getItens(@WebParam(name="filtros") Filtros filtros){
		
		System.out.println("chamou o getItens");
		
		List<Filtro> lista = filtros.getLista();
		List<Item> itensResultado = dao.todosItens(lista);
				
		return new ListaItens(itensResultado);
	}
	
	
	@WebMethod(operationName="cadastrarItem")
	@WebResult(name="item")
	public Item cadastrarItem(	@WebParam(name="usuario", header=true) TokenUsuario token, 
								@WebParam(name="item") Item item) throws AutorizacaoException{
		
		System.out.println("chamou o cadastrarItem" + item + "Token: " + token);
		
		boolean valido = new TokenDao().ehValido(token);
		
		if(!valido){
			throw new AutorizacaoException("Token invalido.");
		}
		this.dao.cadastrar(item);
		
		return item;
	}
	
}
